package com.twuc.bagSaving;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertSame;

class StupidAssistantTest {

    private StupidAssistant getStupidAssistant(Cabinet cabinet) {
        return new StupidAssistant(Collections.singletonList(cabinet));
    }

    private List<CabinetInterface> createCabinets(int number) {
        ArrayList<CabinetInterface> cabinets = new ArrayList<>();

        while (cabinets.size() < number) {
            cabinets.add(CabinetFactory.createCabinetWithPlentyOfCapacity());
        }
        return cabinets;
    }


    @ParameterizedTest
    @EnumSource(value = BagSize.class)
    void given_a_bag_to_stupid_assistant_can_get_a_ticket(BagSize bagSize) {

        Bag bag = new Bag(bagSize);
        Cabinet cabinetWithPlentyOfCapacity = CabinetFactory.createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = getStupidAssistant(cabinetWithPlentyOfCapacity);
        Ticket ticket = stupidAssistant.save(bag);
        Bag foundBag = cabinetWithPlentyOfCapacity.getBag(ticket);
        assertSame(bag, foundBag);
    }


    @ParameterizedTest
    @EnumSource(value = BagSize.class)
    void given_a_bag_to_stupid_assistant_with_3_cabinet_can_get_a_ticket(BagSize bagSize) {

        Bag bag = new Bag(bagSize);
        List<CabinetInterface> cabinets = createCabinets(2);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        Ticket ticket = stupidAssistant.save(bag);
        Bag foundBag = cabinets.get(0).getBag(ticket);
        assertSame(bag, foundBag);
    }


    @ParameterizedTest
    @EnumSource(value = BagSize.class)
    void should_get_the_bag(BagSize bagSize) {
        Bag bag = new Bag(bagSize);
        List<CabinetInterface> cabinets = createCabinets(2);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        Ticket ticket = stupidAssistant.save(bag);
        Bag foundBag = stupidAssistant.getBag(ticket);
        assertSame(bag, foundBag);
    }








}
