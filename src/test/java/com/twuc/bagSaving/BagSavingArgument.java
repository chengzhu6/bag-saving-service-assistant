package com.twuc.bagSaving;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithFullLockers;

@SuppressWarnings("WeakerAccess") // for method source
class BagSavingArgument {
    public static Object[][] createSavableBagSizeAndLockerSize() {
        return new Object[][] {
            new Object[] { BagSize.BIG, LockerSize.BIG },
            new Object[] { BagSize.MEDIUM, LockerSize.BIG },
            new Object[] { BagSize.SMALL, LockerSize.BIG },
            new Object[] { BagSize.MEDIUM, LockerSize.MEDIUM },
            new Object[] { BagSize.SMALL, LockerSize.MEDIUM },
            new Object[] { BagSize.SMALL, LockerSize.SMALL },
        };
    }

    public static Object[][] createNonSavableBagSizeAndLockerSize() {
        return new Object[][] {
            new Object[] { BagSize.BIG, LockerSize.MEDIUM },
            new Object[] { BagSize.BIG, LockerSize.SMALL},
            new Object[] { BagSize.MEDIUM, LockerSize.SMALL},
        };
    }

    public static Object[][] createCabinetWithOnlyOneEmptyLockerAndSavableSizes() {
        return new Object[][] {
            new Object[] {new Cabinet(LockerSetting.of(LockerSize.BIG, 1)), BagSize.BIG, LockerSize.BIG },
            new Object[] {new Cabinet(LockerSetting.of(LockerSize.BIG, 1)), BagSize.MEDIUM, LockerSize.BIG },
            new Object[] {new Cabinet(LockerSetting.of(LockerSize.BIG, 1)), BagSize.SMALL, LockerSize.BIG },
            new Object[] {new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1)), BagSize.MEDIUM, LockerSize.MEDIUM },
            new Object[] {new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1)), BagSize.SMALL, LockerSize.MEDIUM },
            new Object[] {new Cabinet(LockerSetting.of(LockerSize.SMALL, 1)), BagSize.SMALL, LockerSize.SMALL },
        };
    }

    public static Object[][] createCabinetWithOnlyOneLockerSizeFull() {
        return new Object[][] {
            new Object[] {
                createCabinetWithFullLockers(new LockerSize[] {LockerSize.BIG}, 1),
                BagSize.BIG,
                LockerSize.BIG
            },
            new Object[] {
                createCabinetWithFullLockers(new LockerSize[] {LockerSize.BIG}, 1),
                BagSize.MEDIUM,
                LockerSize.BIG
            },
            new Object[] {
                createCabinetWithFullLockers(new LockerSize[] {LockerSize.BIG}, 1),
                BagSize.SMALL,
                LockerSize.BIG
            },
            new Object[] {
                createCabinetWithFullLockers(new LockerSize[] {LockerSize.MEDIUM}, 1),
                BagSize.MEDIUM,
                LockerSize.MEDIUM
            },
            new Object[] {
                createCabinetWithFullLockers(new LockerSize[] {LockerSize.MEDIUM}, 1),
                BagSize.SMALL,
                LockerSize.MEDIUM
            },

            new Object[] {
                createCabinetWithFullLockers(new LockerSize[] {LockerSize.SMALL}, 1),
                BagSize.SMALL,
                LockerSize.SMALL
            }
        };
    }
}
