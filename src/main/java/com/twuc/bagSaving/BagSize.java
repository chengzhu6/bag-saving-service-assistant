package com.twuc.bagSaving;

public enum BagSize {

    SMALL(20),
    MEDIUM(30),
    BIG(40);

    private final int sizeNumber;

    BagSize(int sizeNumber) {
        this.sizeNumber = sizeNumber;
    }

    int getSizeNumber() {
        return sizeNumber;
    }
}
