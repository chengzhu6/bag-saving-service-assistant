package com.twuc.bagSaving;

public interface CabinetInterface{
    Ticket save(Bag bag, LockerSize lockerSize);

    boolean isFull(LockerSize lockerSize);

    Bag getBag(Ticket ticket);

    boolean hasTicket(Ticket ticket);
}
