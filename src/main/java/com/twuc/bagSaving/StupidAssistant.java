package com.twuc.bagSaving;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StupidAssistant {

    private final List<CabinetInterface> cabinets;

    public StupidAssistant(List<CabinetInterface> cabinets) {
        this.cabinets = cabinets;
    }


    private CabinetInterface getEmptyCabinet(LockerSize lockerSize) {
        return cabinets.stream().filter(cabinet ->
                !cabinet.isFull(lockerSize)
        ).findFirst().orElse(null);
    }

    public Ticket save(Bag bag) {
        int bagSizeNumber = bag.getBagSize().getSizeNumber();
        Optional<LockerSize> lockerSize =
                Arrays.stream(LockerSize.values()).filter(item -> item.getSizeNumber() == bagSizeNumber).findFirst();

        if (lockerSize.isPresent()) {
            CabinetInterface emptyCabinet = getEmptyCabinet(lockerSize.get());
            return emptyCabinet.save(bag, lockerSize.get());
        }
        throw new RuntimeException();
    }


    public Bag getBag(Ticket ticket) {
        CabinetInterface savedCabinet = cabinets.stream().filter(cabinet ->
                cabinet.hasTicket(ticket)
        ).findFirst().orElseThrow(() -> new IllegalArgumentException("Invalid ticket."));
        return savedCabinet.getBag(ticket);
    }
}
